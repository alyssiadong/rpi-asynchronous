##email steve@steves-internet-guide.com
##Free to use for any purpose
##If you like and use this code you can
##buy me a drink here https://www.paypal.me/StepenCope
"""
Creates multiple Connections to a broker 
and sends and receives messages.
Uses one thread per client
"""
import paho.mqtt.client as mqtt
import time
import json
import threading
import logging
import queue
import socket
import numpy as np
import numpy.random as rd

logging.basicConfig(level=logging.INFO)

############################################################################## 
## MQTT Client Functions
############################################################################## 

def Connect(client,broker,port,keepalive,run_forever=False):
    """Attempts connection set delay to >1 to keep trying
    but at longer intervals. If runforever flag is true then
    it will keep trying to connect or reconnect indefinetly otherwise
    gives up after 3 failed attempts"""
    connflag=False
    delay=5
    #print("connecting ",client)
    badcount=0 # counter for bad connection attempts
    while not connflag:
        logging.info("connecting to broker "+str(broker))
        #print("connecting to broker "+str(broker)+":"+str(port))
        print("Attempts ",str(badcount))
        time.sleep(delay)
        try:
            client.connect(broker,port,keepalive)
            connflag=True

        except:
            client.bad_connection_flag=True
            logging.info("connection failed "+str(badcount))
            badcount +=1
            if badcount>=3 and not run_forever: 
                return -1
                raise SystemExit #give up
    return 0
    #####end connecting

def wait_for(client,msgType,period=1,wait_time=10,running_loop=False):
    """Will wait for a particular event gives up after period*wait_time, Default=10
    seconds.Returns True if succesful False if fails"""
    #running loop is true when using loop_start or loop_forever
    client.running_loop=running_loop #
    wcount=0  
    while True:
        logging.info("waiting"+ msgType)
        if msgType=="CONNACK":
            if client.on_connect:
                if client.connected_flag:
                    return True
                if client.bad_connection_flag: #
                    return False
        if msgType=="SUBACK":
            if client.on_subscribe:
                if client.suback_flag:
                    return True
        if msgType=="MESSAGE":
            if client.on_message:
                if client.message_received_flag:
                    return True
        if msgType=="PUBACK":
            if client.on_publish:        
                if client.puback_flag:
                    return True
     
        if not client.running_loop:
            client.loop(.01)  #check for messages manually
        time.sleep(period)
        wcount+=1
        if wcount>wait_time:
            print("return from wait loop taken too long")
            return False
    return True

def client_loop(client,broker,port,keepalive=60,loop_function=None,\
    loop_delay=1,run_forever=False):
    """runs a loop that will auto reconnect and subscribe to topics
    pass topics as a list of tuples. You can pass a function to be
    called at set intervals determined by the loop_delay
    """
    client.run_flag=True
    client.broker=broker
    print("running loop ")
    client.reconnect_delay_set(min_delay=1, max_delay=12)
      
    while client.run_flag: #loop forever
        if client.bad_connection_flag:
            break         
        if not client.connected_flag:
            print("Connecting to ",broker)
            if Connect(client,broker,port,keepalive,run_forever) !=-1:
                if not wait_for(client,"CONNACK"):
                   client.run_flag=False #break no connack
            else:#connect fails
                client.run_flag=False #break
                print("quitting loop for  broker ",broker)

        client.loop(0.0001)

        if client.connected_flag and loop_function: #function to call
                loop_function(client,loop_delay) #call function
    time.sleep(1)
    print("disconnecting from",broker)
    if client.connected_flag:
        client.disconnect()
        client.connected_flag=False
    
def on_log(client, userdata, level, buf):
    print(buf)

def on_message(client, userdata, message):
    # print("Message received!")
    mes = json.loads(message.payload)
    t = threading.Thread(target=send_after_delay, args = (client, mes))
    t.daemon = True
    t.start()

def on_connect(client, userdata, flags, rc):
    if rc==0:
        client.connected_flag=True #set flag
        for c in clients:
            print("Subscribing...")
            if client==c["client"]:
                for subtop in c["sub_topic"]:
                    client.subscribe(subtop)
        #print("connected OK")
    else:
        print("Bad connection Returned code=",rc)
        client.loop_stop()  

def on_disconnect(client, userdata, rc):
   client.connected_flag=False #set flag
   #print("client disconnected ok")

def on_publish(client, userdata, mid):
    print("In on_pub callback mid= "  ,mid)

# def pub(client,loop_delay):
#     # print("in publish")
#     for c in clients:
#       if client==c["client"]:
#         time.sleep(loop_delay)
#         msg = "client "+ str(client._client_id) +  " to " + c["pub_topic"]
#         client.publish(c["pub_topic"], msg)

def Create_connections():
    for i in range(nclients):
        cname="client"+str(i)
        t=int(time.time())
        client_id =cname+str(t) #create unique client_id
        client = mqtt.Client(client_id)             #create new instance
        clients[i]["client"]=client 
        clients[i]["client_id"]=client_id
        clients[i]["cname"]=cname
        broker=clients[i]["broker"]
        port=clients[i]["port"]
        client.on_connect = on_connect
        client.on_disconnect = on_disconnect
        #client.on_publish = on_publish
        client.on_message = on_message
        t = threading.Thread(target\
              =client_loop,args=(client,broker,port,60))
        threads.append(t)
        t.start()

def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


############################################################################## 
## Communication Delays Function
##############################################################################

network_topology = {
                        "agents":[i for i in range(9)],
                        "types":["prod", "prod", "prod", 
                                "cons", "cons", "cons", 
                                "cons", "cons", "cons", ],
                        "x_loc":[0.9702303951529305, 0.751174560071338, 0.6933451519086382,
                                0.28889269571746934, 0.2948835269018768, 0.36450347262193783,
                                0.25062913059719416, 0.34941584927361014, 0.040341253413478384,
                                ],
                        "y_loc":[0.086305587456728, 0.24579561557430574, 0.17792194040570708,
                                0.07865965020174959, 0.4842880862004151, 0.8385696331272718,
                                0.18712591869841821, 0.4453448315989057, 0.6041411341279659,
                                ],
                        "alpha":0.0,
                        "beta":0.0,
                        "sigma":0,
}

def compute_delay(i, j):
    xi = network_topology["x_loc"][i]
    xj = network_topology["x_loc"][j]
    yi = network_topology["y_loc"][i]
    yj = network_topology["y_loc"][j]

    alpha = network_topology["alpha"]
    beta = network_topology["beta"]
    sigma0 = network_topology["sigma"]


    dist = np.sqrt( (xi-xj)**2 + (yi-yj)**2)

    mean_delay = alpha*dist+beta
    sigma = mean_delay*sigma0/3

    delay = max(0,mean_delay + sigma*rd.rand())
    return delay

def send_after_delay(client, mes):

    i = mes["from"]
    j = mes["to"]
    delay = compute_delay(i,j)

    if delay > 10:
        print("Warning: delay from {} to {} is {} s".format(i,j, delay))

    if delay != 0:
        time.sleep(delay)
    pub_topic = "T"+str(i)+str(j)+"b"

    client.publish(pub_topic, json.dumps(mes))


clients=[
{"broker":"localhost",
    "port":1883,
    "name":"blank",
    "sub_topic":[
              "T03", "T04", "T05", "T06", "T07", "T08", 
              "T13", "T14", "T15", "T16", "T17", "T18",
              "T23", "T24", "T25", "T26", "T27", "T28", 
              "T30", "T40", "T50", "T60", "T70", "T80", 
              "T31", "T41", "T51", "T61", "T71", "T81",
              "T32", "T42", "T52", "T62", "T72", "T82", ],
    "pub_topic":[]}
]

nclients=len(clients)
message="test message"

mqtt.Client.connected_flag=False #create flag in class
mqtt.Client.bad_connection_flag=False #create flag in class

global queued_messages
queued_messages = queue.Queue()

threads=[]
print("Creating Connections ")
no_threads=threading.active_count()
print("current threads =",no_threads)
print("Publishing ")
Create_connections()

print("All clients connected ")
no_threads=threading.active_count()
print("current threads =",no_threads)
print("starting main loop")
try:
    while True:
        time.sleep(10)
        no_threads=threading.active_count()
        print("current threads =",no_threads)
        for c in clients:
            if not c["client"].connected_flag:
                print("broker ",c["broker"]," is disconnected")
    
except KeyboardInterrupt:
    print("ending")
    for c in clients:
        c["client"].run_flag=False
time.sleep(5)
   
