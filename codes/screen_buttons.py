#!/usr/bin/python
import array, fcntl
from time import sleep
# test program to read state of buttons on 4D Systems 4DPi displays
#LCD4DPI_GET_KEYS = -2147202303
_IOC_NRBITS = 8
_IOC_TYPEBITS = 8
_IOC_SIZEBITS = 14
_IOC_DIRBITS = 2
_IOC_DIRMASK = (1 << _IOC_DIRBITS) - 1
_IOC_NRMASK = (1 << _IOC_NRBITS) - 1
_IOC_TYPEMASK = (1 << _IOC_TYPEBITS ) - 1
_IOC_NRSHIFT = 0
_IOC_TYPESHIFT = _IOC_NRSHIFT+_IOC_NRBITS
_IOC_SIZESHIFT = _IOC_TYPESHIFT+_IOC_TYPEBITS
_IOC_DIRSHIFT = _IOC_SIZESHIFT+_IOC_SIZEBITS
_IOC_NONE = 0
_IOC_WRITE = 1
_IOC_READ = 2

def _IOC(dir, type, nr, size):
	# print 'dirshift {}, typeshift {}, nrshift {}, sizeshift {}'.format(_IOC_DIRSHIFT,
	# _IOC_TYPESHIFT, _IOC_NRSHIFT, _IOC_SIZESHIFT)
	ioc = (dir << _IOC_DIRSHIFT ) | (type << _IOC_TYPESHIFT ) | (nr << _IOC_NRSHIFT ) | (size << _IOC_SIZESHIFT)
	if ioc > 2147483647: ioc -= 4294967296
	return ioc

#def _IO(type, nr):
# 	return _IOC(_IOC_NONE, type, nr, 0)
# 	
def _IOR(type,nr,size):
	return _IOC(_IOC_READ, type, nr, size)
	
#def _IOW(type,nr,size):
# 	return _IOC(_IOC_WRITE, type, nr, sizeof(size))


if __name__ == '__main__':
	LCD4DPI_GET_KEYS = _IOR(ord('K'), 1, 4)
	buf = array.array('h',[0])
	print 'Press Top & Bottom buttons simultaneously to exit'
	with open('/dev/fb1', 'rw') as fd:
		while True:
			fcntl.ioctl(fd, LCD4DPI_GET_KEYS, buf, 1) # execute ioctl call to read the keys
			keys = buf[0]

			if not keys & 0b00001:
				print "KEY1" ,
			if not keys & 0b00010:
				print "KEY2" ,
			if not keys & 0b00100:
				print "KEY3" ,
			if not keys & 0b01000:
				print "KEY4" ,
			if not keys & 0b10000:
				print "KEY5" ,

			if keys != 0b11111:
				print
			if keys == 0b01110: # exit if top and bottom pressed
				break
		 	sleep(0.1)