'''
    Message Buffers
'''
import queue
import numpy as np

def initialize():

	global queued_messages_in 
	global queued_messages_out 
	global message_buffer 
	global message_buffer2 

	global c
	global heartbeat_in
	global heartbeat_out

	global mqtt_flag
	global tState_clientLoop
	global tKill_clientLoop
	global tState_checkConnection
	global tKill_checkConnection

	global tState_heartOut 
	global tState_heartIn 
	global tKill_heartOut 
	global tKill_heartIn 

	global pause_flag
	global tKill_buttonsEvent

	global monitor_out
	global tState_monitor
	global tKill_monitor
	global price
	global k_i
	global primal_res
	global dual_res
	global local_primal_res
	global local_dual_res
	global local_cost
	global local_price
	global local_power
	global message_delays

	queued_messages_in = queue.Queue()
	queued_messages_out = queue.Queue()
	message_buffer = queue.Queue()
	message_buffer2 = queue.Queue()
	c = {}
	heartbeat_in = queue.Queue()
	heartbeat_out = queue.Queue()
	mqtt_flag = False

	tState_clientLoop = False
	tKill_clientLoop = False
	tState_checkConnection = False
	tKill_checkConnection = False

	tState_heartOut = False
	tState_heartIn = False
	tKill_heartOut = False
	tKill_heartIn = False

	pause_flag = False
	tKill_buttonsEvent = False

	monitor_out = queue.Queue()
	tState_monitor = False
	tKill_monitor = False
	k_i = 0
	local_primal_res = 0
	local_dual_res = 0
	local_cost = 0
	local_price = 0
	local_power = 0