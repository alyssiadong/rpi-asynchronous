# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 17:49:11 2019

@author: Alyssia
"""

import osqp
import numpy as np 
from scipy.optimize import minimize, Bounds, LinearConstraint
from scipy import sparse
import pandas as pd

### Setting the agents data for the 'n9_p3_c6_0x0007' testcase.
# agent_data = [
    # {"id_agent":0, "gencost_a":0.03655561276243416, "gencost_b":22.0,\
    #     "Pmin":0, "Pmax":36, "type_agent":"prod",\
    #     "loc_x":0.9702303951529305, "loc_y":0.086305587456728},
    # {"id_agent":1, "gencost_a":0.042827593038154387, "gencost_b":33.0,\
    #     "Pmin":0, "Pmax":307, "type_agent":"prod",\
    #     "loc_x":0.751174560071338, "loc_y":0.24579561557430574},
    # {"id_agent":2, "gencost_a":0.040363596952832634, "gencost_b":32.0,\
    #     "Pmin":0, "Pmax":533, "type_agent":"prod",\
    #     "loc_x":0.6933451519086382, "loc_y":0.17792194040570708},
    # {"id_agent":3, "gencost_a":0.04382892874062356, "gencost_b":81.0,\
    #     "Pmin":-362, "Pmax":0, "type_agent":"cons",\
    #     "loc_x":0.28889269571746934, "loc_y":0.07865965020174959},
    # {"id_agent":4, "gencost_a":0.033642989434490476, "gencost_b":74.0,\
    #     "Pmin":-965, "Pmax":0, "type_agent":"cons",\
    #     "loc_x":0.2948835269018768, "loc_y":0.4842880862004151},
    # {"id_agent":5, "gencost_a":0.030776776340372798, "gencost_b":75.0,\
    #     "Pmin":-1151, "Pmax":0, "type_agent":"cons",\
    #     "loc_x":0.36450347262193783, "loc_y":0.8385696331272718},
    # {"id_agent":6, "gencost_a":0.04127577420178239, "gencost_b":79.0,\
    #     "Pmin":-936, "Pmax":0, "type_agent":"cons",\
    #     "loc_x":0.25062913059719416, "loc_y":0.18712591869841821},
    # {"id_agent":7, "gencost_a":0.0244435040500645, "gencost_b":89.0,\
    #     "Pmin":-127, "Pmax":0, "type_agent":"cons",\
    #     "loc_x":0.34941584927361014, "loc_y":0.4453448315989057},
    # {"id_agent":8, "gencost_a":0.02090970274228856, "gencost_b":69.0,\
    #     "Pmin":-116, "Pmax":0, "type_agent":"cons",\
    #     "loc_x":0.040341253413478384, "loc_y":0.6041411341279659},
    # ]

df = pd.read_csv('n9_p3_c6_0x0007.csv')
df.id_agent = df.id_agent -1 
N_Omega = len(df)

df2 = pd.read_csv('n9_p3_c6_0x0006.csv')
df2.id_agent = df2.id_agent -1


### Agent class
class Agent(object):
    def __init__(self, id_agent):
        # for index,row in df.iterrows():
        #     if row["id_agent"] == id_agent:
        #         self.type = row["type"]
        #         self.id_agent = id_agent

        #         # Agent constants
        #         self.gencost_a = row["gencost_a"]
        #         self.gencost_b = row["gencost_b"]
        #         self.Pmin = row["Pmin"]
        #         self.Pmax = row["Pmax"]
        #         self.loc_x = row["loc_x"]
        #         self.loc_y = row["loc_y"]

        self.id_agent = id_agent

        row = df[df["id_agent"]==id_agent]
        row2 = df2[df2["id_agent"]==id_agent]

        self.type = row["type"][row.index[0]]      # must be the same type
        self.gencost_a = [ row["gencost_a"][row.index[0]], row2["gencost_a"][row.index[0]] ]
        self.gencost_b = [ row["gencost_b"][row.index[0]], row2["gencost_b"][row.index[0]] ]
        self.Pmin = [ row["Pmin"][row.index[0]], row2["Pmin"][row.index[0]] ]
        self.Pmax = [ row["Pmax"][row.index[0]], row2["Pmax"][row.index[0]] ]

        self.param_state = 0

        # Optimization parameters
        self.rho = 0.5
        self.gamma = 1e-3#0.01#1e-3#
        self.epsilon_rel = 1e-9

        # Commercial links
        if self.type == "prod":
            partner_type = "cons"
        else:
            partner_type = "prod"
        self.Omega = set(df["id_agent"])
        omegai = [row["id_agent"] for ind, row in df.iterrows()\
            if row["type"]==partner_type]
        self.omega_i = set(omegai)
        self._omega_i_active = set(omegai)           # sets of all commercial partners that are active

    def __str__(self):
            return "%s   %s " % (self.gencost_a, self.Pmin)

    def initialize(self):
        self.tij = np.zeros(N_Omega)
        self.tij_previous = np.zeros(N_Omega)
        self.tji = np.zeros(N_Omega)
        self.lambdaij = np.zeros(N_Omega)
        self.kij = np.zeros(N_Omega, dtype = int)
        self.ki = 0
        self._prim_res = np.zeros(N_Omega)
        self._dual_res = np.zeros(N_Omega)

    def change_param_state(self):
        if self.param_state == 0:
            self.param_state = 1
        elif self.param_state == 1:
            self.param_state = 0
        else:
            raise ValueError("param_state value must be 0 or 1")

    def tji_update(self, Tji_update):
        for j, tji in Tji_update.items():
            self.tji[j] = tji

    def price_update(self, Phi):
        for j in Phi:
            self.lambdaij[j] = self.lambdaij[j] -\
                self.rho/2*(self.tij[j]+self.tji[j])

    def objective_function(self, tij):
        gencost_a = self.gencost_a[self.param_state]
        gencost_b = self.gencost_b[self.param_state]
        pi = sum(tij)
        tij_k = np.array([self.tij[k] for k in self.omega_i])
        tji_k = np.array([self.tji[k] for k in self.omega_i])
        lambdaij_k = np.array([self.lambdaij[k] for k in self.omega_i])
        return 2*gencost_a*(pi**2) +\
            gencost_b*pi +\
            self.gamma*sum(np.square(tij)) +\
            self.rho/2*sum(np.square((tij_k-tji_k)/2-tij+lambdaij_k/self.rho))

    def jac(self, tij):
        gencost_a = self.gencost_a[self.param_state]
        gencost_b = self.gencost_b[self.param_state]
        tij_k = np.array([self.tij[k] for k in self.omega_i])
        tji_k = np.array([self.tji[k] for k in self.omega_i])
        lambdaij_k = np.array([self.lambdaij[k] for k in self.omega_i])

        d1 = 4*gencost_a*sum(tij)
        d2 = gencost_b
        d3 = 2*self.gamma*tij
        d4 = - self.rho*((tij_k-tji_k)/2-tij+lambdaij_k/self.rho)

        return d1+d2+d3+d4

    def hess(self, tij):
        gencost_a = self.gencost_a[self.param_state]
        n = len(tij)
        H = np.ones((n,n))*4*gencost_a + np.eye(n)*(2*self.gamma+self.rho)
        return H

    def trade_update(self, Phi):

        solver = "OSQP"
        if solver == "optimize":
            Pmin = self.Pmin[self.param_state]
            Pmax = self.Pmax[self.param_state]

            minbound = np.array([Pmin for i in range(len(self.omega_i))])
            maxbound = np.array([Pmax for i in range(len(self.omega_i))])
            bounds = Bounds(minbound, maxbound)

            linear_constraint = LinearConstraint(np.ones((1,len(self.omega_i))),\
                [Pmin], [Pmax])
            tij0 = [self.tij[k] for k in self.omega_i]

            res = minimize(self.objective_function, tij0, method='trust-constr',
                jac = self.jac, hess = self.hess,
                constraints = linear_constraint,
                options={'verbose':0, 'gtol':1e-10, 'xtol':1e-10}, 
                bounds=bounds,
                )
        elif solver == "OSQP":
            gencost_a = self.gencost_a[self.param_state]
            gencost_b = self.gencost_b[self.param_state]
            Pmin = self.Pmin[self.param_state]
            Pmax = self.Pmax[self.param_state]

            n = len(self.omega_i)

            P1 = np.ones((n,n))*2*gencost_a
            P2 = np.eye(n)*(self.rho/2+self.gamma)
            P = sparse.csc_matrix(2*(P1+P2))

            q1 = np.ones(n)*gencost_b
            q2 = -self.rho*np.array([(self.tij[j]-self.tji[j])/2+self.lambdaij[j]/self.rho 
                    for j in self.omega_i])
            q = q1+q2

            A1 = np.eye(n)
            A2 = np.ones((1,n))
            A = sparse.csc_matrix(np.concatenate((A1,A2), axis = 0))

            l1 = [Pmin if (j in self._omega_i_active) else 0 for j in self.omega_i]
            u1 = [Pmax if (j in self._omega_i_active) else 0 for j in self.omega_i]
            l1.append(Pmin) 
            u1.append(Pmax)
            l = np.array(l1)
            u = np.array(u1)

            prob = osqp.OSQP()
            prob.setup(P,q,A,l,u, verbose=False, eps_abs = 1e-12, eps_rel = 1e-12)

            res = prob.solve()
        else:
            raise ValueError

        self.ki +=1

        Tij = {}
        k = 0
        for j in range(N_Omega):
            if j in self.omega_i:
                if j in Phi:
                    if solver == "OSQP":
                        self.tij_previous[j] = self.tij[j]
                        self.tij[j] = res.x[k].item()
                        self.kij[j]+=1
                        Tij[j] = (res.x[k].item(), self.kij[j])
                    else:
                        self.tij_previous[j] = self.tij[j]
                        self.tij[j] = res.x[k]
                        self.kij[j]+=1
                        Tij[j] = (res.x[k], self.kij[j])
                k = k+1

        return Tij

    def compute_primal_res(self):
        self._prim_res = np.square(self.tij + self.tji)

    def compute_dual_res(self):
        self._dual_res = np.square(self.tij - self.tij_previous)

    @property
    def prim_res(self):
        self.compute_primal_res()
        return self._prim_res

    @property
    def dual_res(self):
        self.compute_dual_res()
        return self._dual_res

    @property
    def local_cost(self):
        pi = np.sum(self.tij)
        gencost_a = self.gencost_a[self.param_state]
        gencost_b = self.gencost_b[self.param_state]
        return 2*gencost_a*pi**2 + gencost_b*pi

    @property
    def traded_power(self):
        return np.sum(np.array([self.tij[j] for j in self.omega_i_active]))

    @property
    def local_price(self):
        prices = np.array([self.lambdaij[j] for j in self.omega_i_active])
        return np.mean(prices)

    @property
    def omega_i_active(self):
        return self._omega_i_active

    @omega_i_active.setter
    def omega_i_active(self, values):
        for i in values:
            if i not in self.omega_i:
                raise ValueError("Agent "+str(i)+" is not part of omega_"+str(self.id_agent))
        self._omega_i_active = set(values) 
    
if __name__ == '__main__' :

    A0 = Agent(0)
    A1 = Agent(1)
    A2 = Agent(2)
    A3 = Agent(3)
    A4 = Agent(4)
    A5 = Agent(5)
    A6 = Agent(6)
    A7 = Agent(7)
    A8 = Agent(8)

    A0.initialize()
    A1.initialize()
    A2.initialize()
    A3.initialize()
    A4.initialize()
    A5.initialize()
    A6.initialize()
    A7.initialize()
    A8.initialize()

    for k in range(30):
        A0.price_update(A0.omega_i)
        A1.price_update(A1.omega_i)
        A2.price_update(A2.omega_i)
        A3.price_update(A3.omega_i)
        A4.price_update(A4.omega_i)
        A5.price_update(A5.omega_i)
        A6.price_update(A6.omega_i)
        A7.price_update(A7.omega_i)
        A8.price_update(A8.omega_i)

        T0j = A0.trade_update(A0.omega_i)
        T1j = A1.trade_update(A1.omega_i)
        T2j = A2.trade_update(A2.omega_i)
        T3j = A3.trade_update(A3.omega_i)
        T4j = A4.trade_update(A4.omega_i)
        T5j = A5.trade_update(A5.omega_i)
        T6j = A6.trade_update(A6.omega_i)
        T7j = A7.trade_update(A7.omega_i)
        T8j = A8.trade_update(A8.omega_i)

        Tj0 = {3:T3j[0][0], 4:T4j[0][0], 5:T5j[0][0], 
            6:T6j[0][0], 7:T7j[0][0], 8:T8j[0][0]}
        Tj1 = {3:T3j[1][0], 4:T4j[1][0], 5:T5j[1][0], 
            6:T6j[1][0], 7:T7j[1][0], 8:T8j[1][0]}
        Tj2 = {3:T3j[2][0], 4:T4j[2][0], 5:T5j[2][0], 
            6:T6j[2][0], 7:T7j[2][0], 8:T8j[2][0]}
        Tj3 = {0:T0j[3][0], 1:T1j[3][0], 2:T2j[3][0]}
        Tj4 = {0:T0j[4][0], 1:T1j[4][0], 2:T2j[4][0]}
        Tj5 = {0:T0j[5][0], 1:T1j[5][0], 2:T2j[5][0]}
        Tj6 = {0:T0j[6][0], 1:T1j[6][0], 2:T2j[6][0]}
        Tj7 = {0:T0j[7][0], 1:T1j[7][0], 2:T2j[7][0]}
        Tj8 = {0:T0j[8][0], 1:T1j[8][0], 2:T2j[8][0]}

        A0.tji_update(Tj0)
        A1.tji_update(Tj1)
        A2.tji_update(Tj2)
        A3.tji_update(Tj3)
        A4.tji_update(Tj4)
        A5.tji_update(Tj5)
        A6.tji_update(Tj6)
        A7.tji_update(Tj7)
        A8.tji_update(Tj8)

        print(k)
