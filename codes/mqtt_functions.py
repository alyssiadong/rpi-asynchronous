#! c:\python34\python3
#!/usr/bin/env python
##MQTT demo code provided by Steve Cope at www.steves-internet-guide.com
##email steve@steves-internet-guide.com
##Free to use for any purpose
##If you like and use this code you can
##buy me a drink here https://www.paypal.me/StepenCope


import matplotlib.pyplot as plt
import paho.mqtt.client as mqtt
import numpy as np
import time
import json
import threading
from multiprocessing import Process, active_children, current_process
import logging
from struct import *
import MessageBuffer


###################################################
# MQTT Functions
###################################################
logging.basicConfig(level=logging.INFO)

def Connect(client,broker,port,keepalive,run_forever=False):
    """Attempts connection set delay to >1 to keep trying
    but at longer intervals. If runforever flag is true then
    it will keep trying to connect or reconnect indefinetly otherwise
    gives up after 3 failed attempts"""
    connflag=False
    delay=5
    #print("connecting ",client)
    badcount=0 # counter for bad connection attempts
    while not connflag:
        logging.info("connecting to broker "+str(broker))
        #print("connecting to broker "+str(broker)+":"+str(port))
        print("Attempts ",str(badcount))
        time.sleep(delay)
        try:
            client.connect(broker,port,keepalive)
            connflag=True
            print("client connected")
        except:
            client.bad_connection_flag=True
            logging.info("connection failed "+str(badcount))
            badcount +=1
            if badcount>=3 and not run_forever: 
                return -1
                raise SystemExit #give up
    return 0
    #####end connecting

def wait_for(client,msgType,period=1,wait_time=10,running_loop=False):
    """Will wait for a particular event gives up after period*wait_time, Default=10
    seconds.Returns True if succesful False if fails"""
    #running loop is true when using loop_start or loop_forever
    client.running_loop=running_loop #
    wcount=0  
    while True:
        logging.info("waiting"+ msgType)
        if msgType=="CONNACK":
            if client.on_connect:
                if client.connected_flag:
                    return True
                if client.bad_connection_flag: #
                    return False
        if msgType=="SUBACK":
            if client.on_subscribe:
                if client.suback_flag:
                    return True
        if msgType=="MESSAGE":
            if client.on_message:
                if client.message_received_flag:
                    return True
        if msgType=="PUBACK":
            if client.on_publish:        
                if client.puback_flag:
                    return True
     
        if not client.running_loop:
            client.loop(.01)  #check for messages manually
        time.sleep(period)
        wcount+=1
        if wcount>wait_time:
            print("return from wait loop taken too long")
            return False
    return True

def client_loop(client,broker,port,keepalive=60,loop_function=None,\
    loop_delay=1,run_forever=False):
    """runs a loop that will auto reconnect and subscribe to topics
    pass topics as a list of tuples. You can pass a function to be
    called at set intervals determined by the loop_delay
    """
    client.run_flag=True
    client.broker=broker
    MessageBuffer.tState_clientLoop = True
    # print("running loop ")
    client.reconnect_delay_set(min_delay=1, max_delay=12)
    
    try:
        while client.run_flag and not MessageBuffer.tKill_clientLoop: #loop forever
            if client.bad_connection_flag:
                break         
            if not client.connected_flag:
                # print("Connecting to ",broker)
                if Connect(client,broker,port,keepalive,run_forever) !=-1:
                    if not wait_for(client,"CONNACK",1):
                       client.run_flag=False #break no connack
                else:#connect fails
                    client.run_flag=False #break
                    print("quitting loop for  broker ",broker)

            client.loop(0.001)
            # print("  mqtt client connected ?",client.connected_flag)
            if client.connected_flag and loop_function : #function to call
                    loop_function(client,loop_delay) #call function
    finally:
        time.sleep(1)
        MessageBuffer.tState_clientLoop = False
        print("disconnecting from",broker)
        if client.connected_flag:
            client.disconnect()
            client.connected_flag=False
    
def on_log(client, userdata, level, buf):
   print(buf)

def on_message(client, userdata, message):
    if message.topic == "Heartbeat":
        MessageBuffer.heartbeat_in.put(message)
        # print("size of heartbeat_in queue",MessageBuffer.heartbeat_in.qsize())
    else:
        t_receive = time.time()
        msg_dict = json.loads(message.payload)
        msg_dict["timestamp_receive"] = t_receive
        msg_dict["dt_comm"] = t_receive - msg_dict["timestamp_send"]
        MessageBuffer.queued_messages_in.put(msg_dict)

def on_connect(client, userdata, flags, rc):
    print("on connect !")
    print(MessageBuffer.c)
    print(MessageBuffer.c["sub_topic"])
    if rc==0:
        if MessageBuffer.c["sub_topic"]!="":
            print("subscribing")
            for (key,subtop) in MessageBuffer.c["sub_topic"].items():
                client.subscribe(subtop)
        client.connected_flag=True #set flag["info"]
    else:
        print("Bad connection Returned code=",rc)
        client.loop_stop()  

def on_disconnect(client, userdata, rc):
   client.connected_flag=False #set flag
   #print("client disconnected ok")

def on_publish(client, userdata, mid):
   time.sleep(1)
   print("In on_pub callback mid= "  ,mid)

def pub(client,loop_delay):
    # print(" from mqtt_functions: heartbeat_out.qsize", MessageBuffer.heartbeat_out.qsize())
    # print("   from mqtt_functions:heartbeat queue size:{}".format(MessageBuffer.heartbeat_out.qsize()))
    for k in range(MessageBuffer.queued_messages_out.qsize()):
        # Sending optimization messages
        mes = MessageBuffer.queued_messages_out.get()

        msg_dict = json.loads(mes)
        j = msg_dict["to"]

        client.publish(MessageBuffer.c["pub_topic"][j], mes)
    for k in range(MessageBuffer.heartbeat_out.qsize()):
        # Sending heartbeat
        # print("from mqtt_functions:  Sending heartbeat out there !")
        mes = MessageBuffer.heartbeat_out.get()
        client.publish("Heartbeat", mes)

    for k in range(MessageBuffer.monitor_out.qsize()):
        # Sending monitoring data
        # print("from mqtt_functions: sending monitoring data")
        mes = MessageBuffer.monitor_out.get()
        client.publish("Monitor", mes)

def CreateConnection(clients, ci):
    i = 0
    cname="client"+str(ci)
    t=int(time.time())
    client_id = cname+str(t) #create unique client_id

    client = mqtt.Client(client_id, clean_session=True)             #create new instance
    clients[i]["client"]=client 
    clients[i]["client_id"]=client_id
    clients[i]["cname"]=cname
    broker=clients[i]["broker"]
    port=clients[i]["port"]
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    #client.on_publish = on_publish
    client.on_message = on_message

    t = threading.Thread(target\
        = client_loop,args=(client,broker,port,60,pub,0.01))
    t.daemon = True
    t.start()

    if not MessageBuffer.tState_checkConnection:
        t2 = threading.Thread(target = checkForConnection, args = [client])
        t2.daemon = True
        t2.start()

    return True

def checkForConnection(client):
    MessageBuffer.tState_checkConnection = True
    try:
        while not MessageBuffer.tKill_checkConnection:
            time.sleep(5)
            # print("         connected flag:", client.connected_flag)
            # print("         thread is alive:", MessageBuffer.tState_clientLoop)
            MessageBuffer.mqtt_flag = client.connected_flag and MessageBuffer.tState_clientLoop
    finally:
        MessageBuffer.tState_checkConnection = False