#! c:\python34\python3
#!/usr/bin/env python
"""

"""
import matplotlib
matplotlib.use('TkAgg')
# matplotlib.rcParams['toolbar'] = 'None' 

import matplotlib.pyplot as plt
import paho.mqtt.client as mqtt
import numpy as np
import array, fcntl
import time
import json
import threading
from multiprocessing import Process
import logging
import queue
import socket
from transitions import Machine, State
from struct import *
from agent import Agent 
from mqtt_functions import CreateConnection
import MessageBuffer
from screen_buttons import _IOR

logging.getLogger('transitions').setLevel(logging.ERROR)

'''
Utilitary functions
'''
def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]

def empty_into(queueA, queueB):
    """
    Empty the content of queueA into queueB

    :param      queueA:  The queue a
    :type       queueA:  Queue.queue()
    :param      queueB:  The queue b
    :type       queueB:  Queue.queue()
    """
    n_items = queueA.qsize()
    for n in range(n_items):
        mes = queueA.get()
        queueB.put(mes)

def empty_queue(queueA):
    """
    Empty the content of queueA

    :param      queueA:  The queue a
    :type       queueA:  Queue.queue()
    """
    n_items = queueA.qsize()
    for n in range(n_items):
        mes = queueA.get()

def send_monitoring_data(AgentA):
    """
    Loops in a thread. Sends monitoring data of the
    local agent to the MQTT broker.

    :param      AgentA:  The local agent.
    :type       AgentA:  Agent.agent()
    """
    print("Sending monitoring data...")
    try:
        while not MessageBuffer.tKill_monitor:
            if MessageBuffer.pause_flag:
                local_power = 0
            else:
                local_power = MessageBuffer.local_power
            dict_mes = {
                        "i":AgentA.id_agent,
                        "k_i":MessageBuffer.k_i,
                        "local_price":MessageBuffer.local_price,
                        "local_cost":MessageBuffer.local_cost,
                        "local_primal_res":MessageBuffer.local_primal_res,
                        "local_dual_res":MessageBuffer.local_dual_res,
                        "local_power":local_power,
                        "pause":MessageBuffer.pause_flag,
            }
            mes = json.dumps(dict_mes)
            MessageBuffer.monitor_out.put(mes)

            time.sleep(1)
            if (AgentA.running_flag == False):
                print("Local agent not running: stopping monitoring.")
                break
    finally:
        MessageBuffer.tState_monitor = False
        print("Monitoring data stopped !")

def send_heartbeat(AgentA, partners_flag):
    """
    Loops in a thread. Sends a heartbeat from the local AgentA, every 5 seconds,
    to the MQTT topic 'Heartbeat'. Stops when AgentA's running flag becomes
    False. Not sending any heartbeat if the state machine is paused.
    
    :param      AgentA:  The local agent
    :type       AgentA:  agent.Agent()
    """
    print("Starting heartbeat...")
    DELTA_T = 3 #s
    try:
        while not MessageBuffer.tKill_heartOut:
            if not MessageBuffer.pause_flag:
                send_time = time.time()
                dict_mes = {
                            "i":AgentA.id_agent, 
                            "send_time":send_time,
                            "agent_state":True,
                            "all_partners_flag":all(partners_flag.values()),
                }
                mes = json.dumps(dict_mes)
                MessageBuffer.heartbeat_out.put(mes)
                # print("   sending heartbeat! queue size:{}".format(MessageBuffer.heartbeat_out.qsize()))
                time.sleep(DELTA_T)
            if AgentA.running_flag == False:
                print("Local agent not running: stopping heartbeat.")
                break
    finally:
        MessageBuffer.tState_heartOut = False
        print("Agent heartbeat stopped !")

def receive_heartbeat(AgentA, partners_last_heartbeat, partners_flag, all_partners_flag):
    """
    Loops in a thread. Receives heartbeats from the MQTT topic 'Heartbeat' and
    process the data: stores the last heartbeat timestamp in the
    'partners_last_heartbeat' dictionnary and updates the 'partners_flag'
    dictionnary. If the difference between agent j's last timestamp and the
    current local time is greater than DELTA_T_MAX, AgentA considers that agent
    j is no longer on the market. Stops when AgentA's running flag becomes
    False.
    
    :param      AgentA:                   The local agent
    :type       AgentA:                   agent.Agent()
    :param      partners_last_heartbeat:  The  dictionnary that stores {j :
                                          Double(j last heartbeat timestamp)}, j
                                          being AgentA's trading partner
    :type       partners_last_heartbeat:  Dict()
    :param      partners_flag:            The dictionnary that stores {j :
                                          Bool(AgentA's estimation of j's
                                          state)}, j being AgentA's trading
                                          partner
    :type       partners_flag:            Dict()
    :param      all_partners_flag:        The dictionary that stores the results of 
                                          {j:all(partner_flags.values())[j]}
    :type       all_partners_flag:        Dict()
    """     

    print("Listening to heartbeats...")
    DELTA_T_MAX = 4 #in seconds
    try:
        while not MessageBuffer.tKill_heartIn:
            # print("listening to heartbeats")
            # Update received heartbeats
            for k in range(MessageBuffer.heartbeat_in.qsize()):
                mes = MessageBuffer.heartbeat_in.get()
                # j, send_time, state = unpack('hdh', mes.payload)
                dict_mes = json.loads(mes.payload)
                j = dict_mes["i"]
                send_time = dict_mes["send_time"]
                state = dict_mes["agent_state"]
                all_partners_flag[j] = dict_mes["all_partners_flag"]
                # print("   processing heartbeat {}".format(j))
                if j in partners_last_heartbeat.keys() and state and send_time>partners_last_heartbeat[j]:
                    partners_last_heartbeat[j] = send_time
                elif j in partners_last_heartbeat.keys() and not state and send_time>partners_last_heartbeat[j]:
                    print("    Agent {} is not well...".format(j))
            
            # Determines if partner is still active
            for (j,last_heartbeat) in partners_last_heartbeat.items():
                if last_heartbeat > 0 :
                    current_time = time.time()
                    delta_t = current_time - last_heartbeat
                    if delta_t > DELTA_T_MAX:
                        partners_flag[j] = False
                        print("        agent {} delta_t:{}".format(j,delta_t))
                    else:
                        partners_flag[j] = True

            time.sleep(0.1)
            if AgentA.running_flag == 0:
                break
    finally:
        MessageBuffer.tState_heartIn = False
        print("No longer listening to heartbeats.")

def button_event(AgentA):
    """
    Loops in a thread. Reads the io buffer assigned to the screen buttons. A
    press of KEY5 pauses the state machine, while a press of KEY4 makes it start
    again.
    
    :param      AgentA:  The agent a
    :type       AgentA:  agent.Agent()
    """
    LCD4DPI_GET_KEYS = _IOR(ord('K'), 1, 4)
    buf = array.array('h',[0])
    with open('/dev/fb1', 'rw') as fd:
        while not MessageBuffer.tKill_buttonsEvent:
            fcntl.ioctl(fd, LCD4DPI_GET_KEYS, buf, 1) # execute ioctl call to read the keys
            keys = buf[0]

            if not keys & 0b00001:
                print("KEY1, not assigned yet.")
            if not keys & 0b00010:
                print("KEY2, not assigned yet.")
            if not keys & 0b00100:
                print("   Changing agent parameters")
                AgentA.change_param_state()
            if not keys & 0b01000:
                if MessageBuffer.pause_flag:
                    print("   Ending pause")
                    MessageBuffer.pause_flag = False
            if not keys & 0b10000:
                if not MessageBuffer.pause_flag:
                    print("   Starting pause")
                    MessageBuffer.pause_flag = True

            if keys != 0b11111:
                print
            # if keys == 0b01110: # exit if top and bottom pressed
            #     break
            time.sleep(0.1)
    print("  Stopped button event thread...")

def update_screen(fig, line1, y):
    """
    Loops in a thread. Updates the RPi screen with the latest 
    information contained in 'y'.

    :param      fig:    The figure handle
    :type       fig:    { type_description }
    :param      line1:  The power line handle
    :type       line1:  { type_description }
    :param      y:      The power values
    :type       y:      { type_description }
    """
    print("Updating screen... ")
    try:
        while True:
            line1.set_ydata(y)
            fig.canvas.draw()
    finally:
        print("No longer updating screen.")

'''
Raspberry Pi Cluster Agent class
State Machine
'''
class RPiAgent(object):
    def __init__(self):
        """
        Constructs a new instance.
        """
        self.state_flag = True

    '''
    Add state callbacks
    '''
    def enter_GlobalInit(self):
        """
        Callback from entering state GlobalInit.
        """
        print("Starting global initialization...")

        # Flag init
        mqtt.Client.connected_flag=False #create flag in class
        mqtt.Client.bad_connection_flag=False #create flag in class
        Agent.running_flag = False

        # Buffers init
        MessageBuffer.initialize()
        empty_queue(MessageBuffer.queued_messages_in)
        empty_queue(MessageBuffer.queued_messages_out)
        empty_queue(MessageBuffer.message_buffer)
        empty_queue(MessageBuffer.message_buffer2)
        empty_queue(MessageBuffer.heartbeat_in)
        empty_queue(MessageBuffer.heartbeat_out)
        empty_queue(MessageBuffer.monitor_out)

        # Agent parameters
        ip_address = get_ip_address()
        if ip_address == '192.168.0.111':   # Agent 0
            self.clients=[
            {
                "broker":"192.168.0.129",
                "port":1883,
                "name":"blank",
                "sub_topic":{"heartbeat":"Heartbeat", 3:"T30b", 4:"T40b", 5:"T50b", 6:"T60b", 7:"T70b", 8:"T80b"},
                "pub_topic":{3:"T03", 4:"T04", 5:"T05", 6:"T06", 7:"T07", 8:"T08"},
                "n_trig":1
            }  ]
            self.i = [0]
        elif ip_address == '192.168.0.112': # Agent 1
            self.clients=[
            {
                "broker":"192.168.0.129",
                "port":1883,
                "name":"blank",
                "sub_topic":{"heartbeat":"Heartbeat", 3:"T31b", 4:"T41b", 5:"T51b", 6:"T61b", 7:"T71b", 8:"T81b"},
                "pub_topic":{3:"T13", 4:"T14", 5:"T15", 6:"T16", 7:"T17", 8:"T18"},
                "n_trig":1
            }  ]
            self.i = [1]
        elif ip_address == '192.168.0.113': # Agent 2
            self.clients=[
            {
                "broker":"192.168.0.129",
                "port":1883,
                "name":"blank",
                "sub_topic":{"heartbeat":"Heartbeat", 3:"T32b", 4:"T42b", 5:"T52b", 6:"T62b", 7:"T72b", 8:"T82b"},
                "pub_topic":{3:"T23", 4:"T24", 5:"T25", 6:"T26", 7:"T27", 8:"T28"},
                "n_trig":1
            }  ]
            self.i = [2]
        elif ip_address == '192.168.0.114': # Agent 3
            self.clients=[
            {
                "broker":"192.168.0.129",
                "port":1883,
                "name":"blank",
                "sub_topic":{"heartbeat":"Heartbeat", 0:"T03b", 1:"T13b", 2:"T23b"},
                "pub_topic":{0:"T30", 1:"T31", 2:"T32"},
                "n_trig":1
            }  ]
            self.i = [3]
        elif ip_address == '192.168.0.115': # Agent 4
            self.clients=[
            {
                "broker":"192.168.0.129",
                "port":1883,
                "name":"blank",
                "sub_topic":{"heartbeat":"Heartbeat", 0:"T04b", 1:"T14b", 2:"T24b"},
                "pub_topic":{0:"T40", 1:"T41", 2:"T42"},
                "n_trig":1
            }  ]
            self.i = [4]
        elif ip_address == '192.168.0.116': # Agent 5
            self.clients=[
            {
                "broker":"192.168.0.129",
                "port":1883,
                "name":"blank",
                "sub_topic":{"heartbeat":"Heartbeat", 0:"T05b", 1:"T15b", 2:"T25b"},
                "pub_topic":{0:"T50", 1:"T51", 2:"T52"},
                "n_trig":1
            }  ]
            self.i = [5]
        elif ip_address == '192.168.0.117': # Agent 6
            self.clients=[
            {
                "broker":"192.168.0.129",
                "port":1883,
                "name":"blank",
                "sub_topic":{"heartbeat":"Heartbeat", 0:"T06b", 1:"T16b", 2:"T26b"},
                "pub_topic":{0:"T60", 1:"T61", 2:"T62"},
                "n_trig":1
            }  ]
            self.i = [6]
        elif ip_address == '192.168.0.118': # Agent 7
            self.clients=[
            {
                "broker":"192.168.0.129",
                "port":1883,
                "name":"blank",
                "sub_topic":{"heartbeat":"Heartbeat", 0:"T07b", 1:"T17b", 2:"T27b"},
                "pub_topic":{0:"T70", 1:"T71", 2:"T72"},
                "n_trig":1
            }  ]
            self.i = [7]
        elif ip_address == '192.168.0.119': # Agent 8
            self.clients=[
            {
                "broker":"192.168.0.129",
                "port":1883,
                "name":"blank",
                "sub_topic":{"heartbeat":"Heartbeat", 0:"T08b", 1:"T18b", 2:"T28b"},
                "pub_topic":{0:"T80", 1:"T81", 2:"T82"},
                "n_trig":1
            }  ]
            self.i = [8]
        else:                               # error
            raise ValueError('Unknown ip address.')

        MessageBuffer.c = self.clients[0].copy()

        print("    Initializing agent {}".format(self.i[0]))
        self.A = Agent(self.i[0])
        self.A.initialize()
        self.Omega = set(self.A.Omega)
        self.omega_i = set(self.A.omega_i)
        self.N_omega_i = len(self.omega_i)
        self.n_iter = np.array([0])
        self.n_trig = self.clients[0]["n_trig"]
        self.partners_last_heartbeat = {j : -1 for j in self.omega_i}     # Last time this agent received a heartbeat from its partners
        self.partners_flag = {j : False for j in self.omega_i}
        self.all_partners_flag = {j : False for j in self.Omega}


        self.timestamps_receive = {j: 0 for j in self.omega_i}
        self.dt_comms = {j: 0 for j in self.omega_i}
        self.iteration_timestamp = 0
        self.computation_timestamp = 0
        self.stall_timestamp = 0
        self.dt_log = 0

        self.Tij = {}
        self.Tji = {}
        self.Phi = set()

        self.kError = 0
        self.kErrorMax = 3
        self.kAgentInit = 0
        self.kAgentInit_MAX = 10

        MessageBuffer.tState_heartOut = False
        MessageBuffer.tState_heartIn = False
        MessageBuffer.tKill_heartOut = False
        MessageBuffer.tKill_heartIn = False
        MessageBuffer.tKill_buttonsEvent = False
        MessageBuffer.tKill_monitor = False

        # # Initialize figure and screen thread
        # plt.ion()
        # self.fig = plt.figure()
        # self.ax = self.fig.add_subplot(111)
        # print("    Launched graphical output")

        # axes = plt.gca()
        # axes.set_xlim([0,100])
        # axes.set_ylim([-200,220])

        # self.X_window = 300
        # self.x = np.linspace(0, self.X_window, self.X_window+1)
        # self.y = np.full(self.X_window+1, np.nan)
        # self.y[0] = 0
        # self.line1, = self.ax.plot(self.x, self.y, 'b-')
        # self.fig.canvas.draw()

        # self.t_screen = threading.Thread(target = update_screen, args = (self.fig, self.line1, self.y))
        # self.t_screen.daemon = True
        # self.t_screen.start()
        
        # Buttons
        self.t_bEvent = threading.Thread(target = button_event, args = [self.A])
        self.t_bEvent.daemon = True
        self.t_bEvent.start()

        self.state_flag = True

    def enter_BrokerConnection(self):
        """
        Callback from entering state BrokerConnection.
        """
        print("Starting connection with broker...")
        self.kBrokerConnection = 0
        self.kBrokerConnection_MAX = 30

        MessageBuffer.tKill_clientLoop = False
        MessageBuffer.tKill_checkConnection = False

        CreateConnection(self.clients, self.i[0])

        self.state_flag = True

    def enter_AgentInit(self):
        """
        Callback from entering state AgentInit.
        """
        print("Initializing local agent communication...")

        # self.kAgentInit = 0
        self.kAgentInit_MAX = 3

        MessageBuffer.tKill_heartIn = False
        MessageBuffer.tKill_heartOut = False
        MessageBuffer.tKill_monitor = False

        self.A.running_flag = True
        self.partners_flag = {j : False for j in self.omega_i}

        # Launching process to listen to heartbeats
        self.t_hin = threading.Thread(target\
        = receive_heartbeat,args=(self.A, self.partners_last_heartbeat, self.partners_flag, self.all_partners_flag))
        self.t_hin.daemon = True
        self.t_hin.start()
        MessageBuffer.tState_heartIn = True

        # Launching process to send heartbeats
        if not MessageBuffer.tState_heartOut:
            self.t_hout = threading.Thread(target\
            = send_heartbeat,args=(self.A, self.partners_flag))
            self.t_hout.daemon = True
            self.t_hout.start()
            MessageBuffer.tState_heartOut = True

        # Launching monitoring process
        if not MessageBuffer.tState_monitor:
            self.t_mon = threading.Thread(target\
            = send_monitoring_data, args = [self.A])
            self.t_mon.daemon = True
            self.t_mon.start()
            MessageBuffer.tState_monitor = True

        # Making sure every commercial partners are alive
        self.heartbeat_init_flag = self.waitForFirstHeartbeat()

        if self.heartbeat_init_flag:
            # Sending first messages
            self.A.price_update(self.omega_i)
            Tij = self.A.trade_update(self.omega_i)
            i = self.i[0]
            ki = self.A.ki
            prim_res = self.A.prim_res
            dual_res = self.A.dual_res
            lambdaij = self.A.lambdaij
            t0 = time.time()
            for (j,item) in Tij.items():
                tij, kij = item
                dict_msg = {
                                "from":i,
                                "to":j,
                                "trade_ij":tij,
                                "k_ij":kij,
                                "timestamp_send":t0,
                                "ki":ki,
                                "primal_res_ij":prim_res[j],
                                "dual_res_ij":dual_res[j],
                                "price_ij":lambdaij[j],
                                "previous_mes_delay":-1,
                }
                msg = json.dumps(dict_msg)
                MessageBuffer.queued_messages_out.put(msg)
            print("   First messages sent.")

        self.state_flag = True

    def enter_Stall(self):
        """
        Callback from entering state Stall.
        """
        print("Waiting for messages...")

        self.stall_timestamp = time.time()

        self.stall_flag = False
        self.Tji = {}
        self.Phi = set()
        res = self.waitForMessages()

        if not all(self.partners_flag.values()):
            print("     not all partners are there", self.partners_flag)

        if res == False:
            self.stall_flag = False
        else:
            self.Tji, self.Phi = res
            self.stall_flag = True
        self.state_flag = True

    def enter_Computation(self):
        """
        Callback from entering state Computation.
        """
        print("Iteration computation...")
        self.computation_flag = False

        self.computation_timestamp = time.time()

        self.Tij = {}
        self.computation_flag = self.AgentIteration()

        # # Update plot
        # self.updatePlot()

        # Update monitoring data
        self.updateMonitoringData()

        self.t0_log = time.time()
        self.state_flag = True

    def enter_Sendback(self):
        """
        Callback from entering state Sendback.
        """
        print("Send back messages...")

        self.dt_log = time.time() - self.t0_log

        i = self.i[0]
        ki = self.A.ki
        prim_res = self.A.prim_res
        dual_res = self.A.dual_res
        lambdaij = self.A.lambdaij
        for (j,item) in self.Tij.items():
            t0 = time.time()
            tij, kij = item
            dict_msg = {
                            "from":i,
                            "to":j,
                            "trade_ij":tij,
                            "k_ij":kij,
                            "ki":ki,
                            "primal_res_ij":prim_res[j],
                            "dual_res_ij":dual_res[j],
                            "price_ij":lambdaij[j],
                            "timestamp_send":t0,
                            "dt_comm":self.dt_comms[j],
                            "dt_stall":self.computation_timestamp - self.timestamps_receive[j],
                            "dt_comp": t0 - self.computation_timestamp,
                            "dt_iteration": self.dt_iteration,
                            "dt_log":self.dt_log,
            }
            msg = json.dumps(dict_msg)
            MessageBuffer.queued_messages_out.put(msg)

        self.state_flag = True

    def enter_Pause(self):
        """
        Callback from entering state Pause.
        """
        print("Pause...")
        self.state_flag = True

    def enter_Error(self):
        """
        Callback from entering state Error.
        """
        print("Error!")
        raise SystemExit

    def exit_State(self):
        """
        Callback from exiting any state.
        """
        self.state_flag = False

    '''
    Local functions
    '''
    def waitForFirstHeartbeat(self):
        # Waits for all of agent A partners first heartbeat
        k = 0
        KMAX = 30

        cond = all(self.partners_flag.values()) and all(self.all_partners_flag.values())

        while not cond:
            time.sleep(1)
            cond = all(self.partners_flag.values()) and all(self.all_partners_flag.values())
            k = k+1
            if k > KMAX:
                print("   Timeout: not all first heartbeats were heard.")
                print(self.partners_flag)
                return False
        return True

    def waitForMessages(self):
        """
        Function called in 'Stall' state.
        Tries for 60 seconds to wait for the appropriate 
        number of messages n_trig such that kij = kji.
        Put back unfit messages in incoming message buffer.
        If one of the partner's flag is False, then put back any of
        its message in incoming message buffer and set tji = 0.
        Returns dictionnary containing new tji values: Tji = {j:tji}
        and the set Phi_i = Phi = {j}
        """

        k = 0
        KMAX = 60*1000
        while self.A.running_flag:
            time.sleep(0.01)
            k = k+1
            if k>KMAX:
                break
            n_inbox = MessageBuffer.queued_messages_in.qsize()

            Tji = {}
            Phi = set()

            if n_inbox != 0 :
                kij = self.A.kij
                for k in range(n_inbox):
                    msg_dict = MessageBuffer.queued_messages_in.get()
                    kji = msg_dict["k_ij"]
                    tji = msg_dict["trade_ij"]
                    j = msg_dict["from"]
                    dt_comm = msg_dict["dt_comm"]
                    t1 = msg_dict["timestamp_receive"]

                    if (kji != kij[j]) or not(self.partners_flag[j]):
                        MessageBuffer.message_buffer.put(msg_dict)             # message_buffer contains kji > kij messages
                    else:
                        Tji[j] = tji
                        Phi.update([j])
                        self.dt_comms[j] = dt_comm
                        self.timestamps_receive[j] = t1
                        MessageBuffer.message_buffer2.put(msg_dict)            # message_buffer2 contains kji == kij messages
                empty_into(MessageBuffer.message_buffer, MessageBuffer.queued_messages_in)

                # If there is enough messages to trigger next iteration
                if ((self.A.ki != 0) and (len(Phi) >= self.n_trig)) or ((self.A.ki == 0) and len(Phi) >= len(self.omega_i)):
                    with MessageBuffer.message_buffer2.mutex:
                        MessageBuffer.message_buffer2.queue.clear()
                    return Tji, Phi
                else:
                    empty_into(MessageBuffer.message_buffer2, MessageBuffer.queued_messages_in)
            
        return False

    def AgentIteration(self):
        """
        Performs price and trade update of local agent.
        Updates the dictionnary Tij containing: {j: (tij, kij)}.
    
        :returns:   True if computation went without error.
        :rtype:     Bool()
        """

        self.n_iter[0] = self.n_iter[0] + 1

        try:
            self.iteration_timestamp = time.time()

            # Updating tji
            self.A.tji_update(self.Tji)

            # Active partners
            self.A.omega_i_active = [j for j in self.partners_flag.keys() if self.partners_flag[j]]

            # Price update
            self.A.price_update(self.Phi)
            price = sum(self.A.lambdaij)/self.N_omega_i

            # Trade update
            self.Tij = self.A.trade_update(self.Phi)

            self.dt_iteration = time.time() - self.iteration_timestamp
        except:
            return False

        # Print local results
        self.power = sum(self.A.tij)
        print("   n:{},   Power :{}, Price:{}".format(self.n_iter[0],self.power, price))

        return True

    def updatePlot(self):
        """
        Updates the figure plotted on the RPi screen with 
        latest information of the local traded power.
        """
        n = self.n_iter[0]
        if n < self.X_window:
            self.y[n] = self.power
        
    def updateMonitoringData(self):
        MessageBuffer.local_primal_res = np.sum(self.A.prim_res)
        MessageBuffer.local_dual_res = np.sum(self.A.dual_res)
        MessageBuffer.local_cost = self.A.local_cost
        MessageBuffer.local_price = self.A.local_price
        MessageBuffer.local_power = self.A.traded_power
        MessageBuffer.k_i = self.A.ki


    '''
    Transitions callbacks
    '''
    def sleep(self):
        time.sleep(10)

    def callbackBrokerConnection1(self):
        # Callback on the transition between BrokerConnection and AgentInit
        self.kBrokerConnection = 0

    def callbackBrokerConnection2(self):
        # Callback on the transition between BrokerConnection and BrokerConnection
        self.kBrokerConnection = 0
        self.kError += 1
        MessageBuffer.tKill_clientLoop = True
        time.sleep(5)

    def callbackBrokerConnection3(self):
        # Callback on the transition between BrokerConnection and Error
        self.kBrokerConnection = 0
        MessageBuffer.tKill_clientLoop = True
        MessageBuffer.tKill_checkConnection = True
        time.sleep(5)

    def callbackAgentInit1(self):
        # Callback on the transition between AgentInit and Stall
        self.kAgentInit = 0

    def callbackAgentInit2(self):
        # Callback on the transition between AgentInit and AgentInit
        # self.t_hin.terminate()
        # self.t_hout.terminate()
        MessageBuffer.tKill_heartIn = True
        print(" callback agentinit 2")
        time.sleep(5)

    def callbackAgentInit3(self):
        # Callback on the transition between AgentInit and BrokerConnection
        # Callback on the transition between AgentInit and Error
        self.kAgentInit = 0
        self.kError += 1

        MessageBuffer.tKill_heartIn = True
        MessageBuffer.tKill_heartOut = True
        MessageBuffer.tKill_monitor = True
        MessageBuffer.tKill_clientLoop = True
        MessageBuffer.tKill_checkConnection = True
        print(" callback agentinit 3")

        time.sleep(10)

    '''
    Transitions conditions
    '''
    def condStateFlag(self):
        return self.state_flag

    def condBrokerConnection1(self):
        # Condition on the transition between BrokerConnection and AgentInit
        if self.kError < self.kErrorMax:
            if MessageBuffer.mqtt_flag:
                return True
            else:
                self.kBrokerConnection +=1
                print("    kBrokerConnection:{}".format(self.kBrokerConnection))
                time.sleep(1)
                return False
        else:
            return False

    def condBrokerConnection2(self):
        # Condition on the transition between BrokerConnection and BrokerConnection
        if self.kError < self.kErrorMax:
            return (self.kBrokerConnection > self.kBrokerConnection_MAX)
        else:
            return False

    def condBrokerConnection3(self):
        # Condition on the transition between BrokerConnection and Error
        return (self.kError >= self.kErrorMax)

    def condAgentInit1(self):
        # Condition on the transition between AgentInit and Stall
        if self.kError < self.kErrorMax:
            return self.heartbeat_init_flag
        else:
            return False

    def condAgentInit2(self):
        # Condition on the transition between AgentInit and AgentInit
        if self.kError < self.kErrorMax:
            if (not self.heartbeat_init_flag) and (self.kAgentInit < self.kAgentInit_MAX):
                self.kAgentInit +=1
                return True
            else:
                return False
        else:
            return False

    def condAgentInit3(self):
        # Condition on the transition between AgentInit and BrokerConnection
        if (not self.heartbeat_init_flag) and (self.kAgentInit >= self.kAgentInit_MAX) and (self.kError < self.kErrorMax):
            return True
        else:
            return False

    def condStall(self):
        # Condition on the transition between Stall and Computation
        return self.stall_flag

    def condPause(self):
        # print("    pause flag", MessageBuffer.pause_flag)
        return MessageBuffer.pause_flag

    def condComputation(self):
        # Condition on the transition between Computation and Sendback
        return self.computation_flag

states = [
                State(name='Init0'),
                State(name='GlobalInit', on_enter=['enter_GlobalInit'], on_exit=['exit_State']),
                State(name='BrokerConnection', on_enter=['enter_BrokerConnection'], on_exit=['exit_State']),
                State(name='AgentInit', on_enter=['enter_AgentInit'], on_exit=['exit_State']),
                State(name='Stall', on_enter=['enter_Stall'], on_exit=['exit_State']),
                State(name='Computation', on_enter=['enter_Computation'], on_exit=['exit_State']),
                State(name='Sendback', on_enter=['enter_Sendback'], on_exit=['exit_State']),
                State(name='Pause', on_enter=['enter_Pause'], on_exit=['exit_State']),
                State(name='Error', on_enter=['enter_Error'])
    ]
transitions = [
    {'trigger':'start', 'source':'Init0', 'dest':'GlobalInit'},
    {'trigger':'next_state', 'source':'GlobalInit', 'dest':'BrokerConnection', 'conditions' : 'condStateFlag'},
    {'trigger':'next_state', 'source':'BrokerConnection', 'dest':'AgentInit', 
        'conditions' : ['condStateFlag', 'condBrokerConnection1'], 'before':'callbackBrokerConnection1' },
    {'trigger':'next_state', 'source':'BrokerConnection', 'dest':'BrokerConnection', 
        'conditions' : ['condStateFlag', 'condBrokerConnection2'], 'before':'callbackBrokerConnection2'},
    {'trigger':'next_state', 'source':'AgentInit', 'dest':'Stall', 
        'conditions' : ['condStateFlag', 'condAgentInit1'], 'before':'callbackAgentInit1'},
    {'trigger':'next_state', 'source':'AgentInit', 'dest':'AgentInit', 
        'conditions' : ['condStateFlag', 'condAgentInit2'], 'before':'callbackAgentInit2'},
    {'trigger':'next_state', 'source':'AgentInit', 'dest':'BrokerConnection', 
        'conditions' : ['condStateFlag', 'condAgentInit3'],'before':'callbackAgentInit3'},
    {'trigger':'next_state', 'source':'Stall', 'dest':'Computation', 'conditions' : ['condStateFlag', 'condStall']},
    {'trigger':'next_state', 'source':'Computation', 'dest':'Sendback', 'conditions' : ['condStateFlag', 'condComputation']},
    {'trigger':'next_state', 'source':'Sendback', 'dest':'Stall', 'conditions' : 'condStateFlag', 'unless':'condPause'},
    {'trigger':'next_state', 'source':'Sendback', 'dest':'Pause', 'conditions' : ['condStateFlag', 'condPause']},
    {'trigger':'next_state', 'source':'Pause', 'dest':'Stall', 'conditions' : 'condStateFlag', 'unless':'condPause'},
    {'trigger':'next_state', 'source':'BrokerConnection', 'dest':'Error', 
        'conditions':'condBrokerConnection3','before':'callbackBrokerConnection3'},
    {'trigger':'next_state', 'source':'AgentInit', 'dest':'Error', 'before':'callbackAgentInit3'},
    {'trigger':'next_state', 'source':['Stall', 'Computation', 'Sendback'], 'dest':'Error'},
]

if __name__ == '__main__':

    rpi_local = RPiAgent()
    machine = Machine(model = rpi_local, \
        states = states, transitions = transitions, initial= 'Init0')
    rpi_local.start()
    while True:
        rpi_local.next_state()
        if rpi_local.state == 'BrokerConnection':
            time.sleep(0.05)
        else:
            time.sleep(0.005)
        if rpi_local.state == 'Error':
            break

    print("Local computation stopped.")