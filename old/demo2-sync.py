#! c:\python34\python3
#!/usr/bin/env python
##demo code provided by Steve Cope at www.steves-internet-guide.com
##email steve@steves-internet-guide.com
##Free to use for any purpose
##If you like and use this code you can
##buy me a drink here https://www.paypal.me/StepenCope
"""
Creates multiple Connections to a broker 
and sends and receives messages.
Uses one thread per client
"""
import matplotlib
matplotlib.use('TkAgg')
matplotlib.rcParams['toolbar'] = 'None' 

import matplotlib.pyplot as plt
import paho.mqtt.client as mqtt
import numpy as np
import time
import json
import threading
import logging
import queue
import socket
from struct import *
from agent import Agent 

###################################################
# Auxiliary functions
###################################################
def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]

def empty_into(queueA, queueB):
    # Empty the content of queueA into queueB
    n_items = queueA.qsize()
    for n in range(n_items):
        mes = queueA.get()
        queueB.put(mes)

###################################################
# MQTT Functions
###################################################
logging.basicConfig(level=logging.INFO)

def Connect(client,broker,port,keepalive,run_forever=False):
    """Attempts connection set delay to >1 to keep trying
    but at longer intervals. If runforever flag is true then
    it will keep trying to connect or reconnect indefinetly otherwise
    gives up after 3 failed attempts"""
    connflag=False
    delay=5
    #print("connecting ",client)
    badcount=0 # counter for bad connection attempts
    while not connflag:
        logging.info("connecting to broker "+str(broker))
        #print("connecting to broker "+str(broker)+":"+str(port))
        print("Attempts ",str(badcount))
        time.sleep(delay)
        try:
            client.connect(broker,port,keepalive)
            connflag=True

        except:
            client.bad_connection_flag=True
            logging.info("connection failed "+str(badcount))
            badcount +=1
            if badcount>=3 and not run_forever: 
                return -1
                raise SystemExit #give up
    return 0
    #####end connecting

def wait_for(client,msgType,period=1,wait_time=10,running_loop=False):
    """Will wait for a particular event gives up after period*wait_time, Default=10
    seconds.Returns True if succesful False if fails"""
    #running loop is true when using loop_start or loop_forever
    client.running_loop=running_loop #
    wcount=0  
    while True:
        logging.info("waiting"+ msgType)
        if msgType=="CONNACK":
            if client.on_connect:
                if client.connected_flag:
                    return True
                if client.bad_connection_flag: #
                    return False
                
        if msgType=="SUBACK":
            if client.on_subscribe:
                if client.suback_flag:
                    return True
        if msgType=="MESSAGE":
            if client.on_message:
                if client.message_received_flag:
                    return True
        if msgType=="PUBACK":
            if client.on_publish:        
                if client.puback_flag:
                    return True
     
        if not client.running_loop:
            client.loop(.01)  #check for messages manually
        time.sleep(period)
        wcount+=1
        if wcount>wait_time:
            print("return from wait loop taken too long")
            return False
    return True

def client_loop(client,broker,port,keepalive=60,loop_function=None,\
    loop_delay=1,run_forever=False):
    """runs a loop that will auto reconnect and subscribe to topics
    pass topics as a list of tuples. You can pass a function to be
    called at set intervals determined by the loop_delay
    """
    client.run_flag=True
    client.broker=broker
    print("running loop ")
    client.reconnect_delay_set(min_delay=1, max_delay=12)
      
    while client.run_flag: #loop forever
        if client.bad_connection_flag:
            break         
        if not client.connected_flag:
            print("Connecting to ",broker)
            if Connect(client,broker,port,keepalive,run_forever) !=-1:
                if not wait_for(client,"CONNACK"):
                   client.run_flag=False #break no connack
            else:#connect fails
                client.run_flag=False #break
                print("quitting loop for  broker ",broker)

        client.loop(0.01)

        if client.connected_flag and loop_function and (queued_messages_out.qsize()!=0): #function to call
                loop_function(client,loop_delay) #call function
    time.sleep(1)
    print("disconnecting from",broker)
    if client.connected_flag:
        client.disconnect()
        client.connected_flag=False
    
def on_log(client, userdata, level, buf):
   print(buf)

def on_message(client, userdata, message):
  queued_messages_in.put(message)
  i1, j, tij, kij = unpack('hhdi', message.payload)
  print(("message received ", i1, tij, kij) )

def on_connect(client, userdata, flags, rc):
    if rc==0:
        client.connected_flag=True #set flag
        for c in clients:
          if client==c["client"]:
              if c["sub_topic"]!="":
                for (key,subtop) in c["sub_topic"].items():
                  client.subscribe(subtop)
        #print("connected OK")
    else:
        print("Bad connection Returned code=",rc)
        client.loop_stop()  

def on_disconnect(client, userdata, rc):
   client.connected_flag=False #set flag
   #print("client disconnected ok")

def on_publish(client, userdata, mid):
   time.sleep(1)
   print("In on_pub callback mid= "  ,mid)

def pub(client,loop_delay):
    c = clients[0]
    for k in range(queued_messages_out.qsize()):
        mes = queued_messages_out.get()
        i1,j,tij, kij = unpack('hhdi', mes)
        client.publish(c["pub_topic"][j], mes)

def Create_connections():
    for i in range(nclients):
        cname="client"+str(i)
        t=int(time.time())
        client_id =cname+str(t) #create unique client_id
        client = mqtt.Client(client_id, clean_session=True)             #create new instance
        clients[i]["client"]=client 
        clients[i]["client_id"]=client_id
        clients[i]["cname"]=cname
        broker=clients[i]["broker"]
        port=clients[i]["port"]
        client.on_connect = on_connect
        client.on_disconnect = on_disconnect
        #client.on_publish = on_publish
        client.on_message = on_message
        t = threading.Thread(target\
            =client_loop,args=(client,broker,port,60,pub))
        threads.append(t)
        t.start()

def LaunchProcess():
    for i in range(nclients):
        client = clients[i]["client"]
        t = threading.Thread(target = InboxProcess, args=[client])
        threads.append(t)
        t.start()

###################################################
# Agent iteration and plot function
###################################################
def AgentIteration(i, Tji, Phi):
    n_iter[0] = n_iter[0] + 1
    print("     Agent n_iter:", n_iter[0])

    # Updating tji
    A.tji_update(Tji)

    # Price update
    A.price_update(Phi)

    # Trade update
    Tij = A.trade_update(Phi)

    # Preparing messages to send
    for (j,item) in Tij.items():
        tij, kij = item
        msg = pack('hhdi', i, j, tij, kij)
        queued_messages_out.put(msg)
        print(("tij=", tij, "kij", kij))

    # Print local results
    power = sum(A.tij)
    print(("Power :", power))

    # Update plot
    updatePlot(power)

def updatePlot(power):
    n = n_iter[0]
    if n < X_window:
        y[n] = power

###################################################
# Inbox message function
###################################################
def InboxProcess(client):

    print(" Starting inbox process")
    while client.run_flag and A.running_flag:
        time.sleep(0.01)
        n_inbox = queued_messages_in.qsize()

        Tji = {}
        Phi = set()

        n_trig = clients[0]["n_trig"]

        if n_iter[0] == 0:
            AgentIteration(i[0], Tji, omega_i)

        if n_inbox != 0:
            kij = A.kij
            for k in range(n_inbox):
                mes = queued_messages_in.get()
                j, i1, tji, kji = unpack('hhdi', mes.payload)
                if kji != kij[j]:
                    message_buffer.put(mes)             # message_buffer contains kji > kij messages
                else:
                    Tji[j] = tji
                    Phi.update([j])
                    message_buffer2.put(mes)            # message_buffer2 contains kji == kij messages
            empty_into(message_buffer, queued_messages_in)

            if len(Phi) >= n_trig:
                with message_buffer2.mutex:
                    message_buffer2.queue.clear()
                AgentIteration(i[0], Tji, Phi)
            else:
                empty_into(message_buffer2, queued_messages_in)

def InboxProcessInit():
    success = False
    print(" Sending initial messages to partners")
    for j in omega_i:
        mes = pack('hhdi', i[0], j, 0, 0)
        queued_messages_out.put(mes)

    time.sleep(5)
    print(" Waiting for initial messages from partners")
    for k in range(3):
        print("Try n", k)

        n_inbox = queued_messages_in.qsize()
        if n_inbox >= N_omega_i:
            if readInboxInit():
                success = True
                break
        time.sleep(3)
    return success

def readInboxInit():
    # Returns true if all of agent i's partners
    # init messages are received

    Phi = set()

    n_inbox = queued_messages_in.qsize()
    for k in range(n_inbox):
        mes = queued_messages_in.get()
        message_buffer.put(mes)
        i,j,tij,kij = unpack('hhdi', mes.payload)
        if tij == 0 and kij == 0:
            Phi.update([i])
        else:
            message_buffer2.put(mes)

    if Phi==omega_i:
        empty_into(message_buffer2, queued_messages_in)
        with message_buffer.mutex:
            message_buffer.queue.clear()
        return True
    else:
        empty_into(message_buffer, queued_messages_in)
        with message_buffer2.mutex:
            message_buffer2.queue.clear()
        return False

###################################################
# Script
###################################################

ip_address = get_ip_address()
if ip_address == '192.168.0.112':   # Agent 1
    clients=[
    {"broker":"192.168.0.129","port":1883,"name":"blank","sub_topic":{0:"T01"},"pub_topic":{0:"T10"},
    "n_trig":1}
    ]
    i = [1]

elif ip_address == '192.168.0.113': # Agent 2
    clients=[
    {"broker":"192.168.0.129","port":1883,"name":"blank","sub_topic":{0:"T02"},"pub_topic":{0:"T20"},
    "n_trig":1}
    ]
    i = [2]
else:                               # Agent 0
    clients=[
    {"broker":"localhost","port":1883,"name":"blank","sub_topic":{1:"T10", 2:"T20"},"pub_topic":{1:"T01", 2:"T02"},
    "n_trig":2}
    ]
    i = [0]


nclients=len(clients)

mqtt.Client.connected_flag=False #create flag in class
mqtt.Client.bad_connection_flag=False #create flag in class

queued_messages_in = queue.Queue()
queued_messages_out = queue.Queue()
message_buffer = queue.Queue()
message_buffer2 = queue.Queue()

print("Initializing agent")
Agent.running_flag = False

A = Agent(i[0])
A.initialize()
omega_i = set(A.omega_i)
N_omega_i = len(omega_i)
n_iter = np.array([0])

threads=[]
print("Creating Connections ")
no_threads=threading.active_count()
print("current threads =",no_threads)
print("Publishing ")
Create_connections()

print("All clients connected ")
no_threads=threading.active_count()
print("current threads =",no_threads)

print("Launch graphical output")
plt.ion()
fig = plt.figure()
ax = fig.add_subplot(111)

axes = plt.gca()
axes.set_xlim([0,100])
axes.set_ylim([-200,200])

X_window = 100
x = np.linspace(0, X_window, X_window+1)
y = np.full(X_window+1, np.nan)
y[0] = 0
line1, = ax.plot(x, y, 'b-')
fig.canvas.draw()

time.sleep(20)

print("Initialization step...")
init_success = InboxProcessInit()

if init_success == False:
    print(" Initialization failed !!")
else:
    A.running_flag = True

print("Launch agent process")
LaunchProcess()

time.sleep(3)
print("starting main loop")
try:
    while True:
        time.sleep(0.5)
        # no_threads=threading.active_count()
        # print("current threads =",no_threads)
        # print("message out queue:", queued_messages_out.qsize())
        # print("message in queue:", queued_messages_in.qsize())
        line1.set_ydata(y)
        fig.canvas.draw()
        for c in clients:
            if not c["client"].connected_flag:
                print("broker ",c["broker"]," is disconnected")
except KeyboardInterrupt:
    print("ending")
    for c in clients:
        c["client"].run_flag=False
time.sleep(10)
   
    







