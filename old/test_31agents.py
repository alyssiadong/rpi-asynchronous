import numpy as np 
import pandas
from scipy.optimize import minimize, Bounds, LinearConstraint

df= pandas.read_csv('n31_p10_c21_0xeeee.csv', sep = ';')

### Setting the agents data for the 'n31_p10_c21_0xeeee' testcase.
agent_data = [ {"id_agent": i, "gencost_a":df.gencost_a[i],\
    "gencost_b":df.gencost_b[i], "Pmin":df.Pmin[i], "Pmax":df.Pmax[i],\
    "type_agent":df.type[i], "loc_x":df.loc_x[i], "loc_y":df.loc_y[i]} for i in range(31)]
N_Omega = len(agent_data)

### Agent class
class Agent:
    def __init__(self, id_agent):
        for agent in agent_data:
            if agent["id_agent"] == id_agent:
                self.type = agent["type_agent"]

                # Agent constants
                self.gencost_a = agent["gencost_a"]
                self.gencost_b = agent["gencost_b"]
                self.Pmin = agent["Pmin"]
                self.Pmax = agent["Pmax"]
                self.loc_x = agent["loc_x"]
                self.loc_y = agent["loc_y"]
        # Optimization parameters
        self.rho = 5
        self.gamma = 1#0.01
        self.epsilon_rel = 1e-4

        # Commercial links
        if self.type == "prod":
            partner_type = "cons"
        else:
            partner_type = "prod"
        omegai = [agent["id_agent"] for agent in agent_data\
            if agent["type_agent"]==partner_type]
        self.omega_i = set(omegai)
        self.dist = [np.sqrt((agent["loc_x"]-self.loc_x)**2\
            +(agent["loc_y"]-self.loc_y)**2)\
            for agent in agent_data]

    def __str__(self):
            return "%s   %s " % (self.gencost_a, self.Pmin)

    def initialize(self):
        self.tij = np.zeros(N_Omega)
        self.tji = np.zeros(N_Omega)
        self.lambdaij = np.zeros(N_Omega)
        self.kij = np.zeros(N_Omega, dtype = int)

    def tji_update(self, Tji_update):
        for j, tji in Tji_update.items():
            self.tji[j] = tji

    def price_update(self, Phi):
        for j in Phi:
            self.lambdaij[j] = self.lambdaij[j] -\
                self.rho/2*(self.tij[j]+self.tji[j])

    def objective_function(self, tij):
        pi = sum(tij)
        tij_k = np.array([self.tij[k] for k in self.omega_i])
        tji_k = np.array([self.tji[k] for k in self.omega_i])
        lambdaij_k = np.array([self.lambdaij[k] for k in self.omega_i])
        return 2*self.gencost_a*(pi**2) +\
            self.gencost_b*pi +\
            self.gamma*sum(np.square(tij)) +\
            self.rho/2*sum(np.square((tij_k-tji_k)/2-tij+lambdaij_k/self.rho))

    def jac(self, tij):
        tij_k = np.array([self.tij[k] for k in self.omega_i])
        tji_k = np.array([self.tji[k] for k in self.omega_i])
        lambdaij_k = np.array([self.lambdaij[k] for k in self.omega_i])

        d1 = 4*self.gencost_a*sum(tij)
        d2 = self.gencost_b
        d3 = 2*self.gamma*tij
        d4 = - self.rho*((tij_k-tji_k)/2-tij+lambdaij_k/self.rho)

        return d1+d2+d3+d4

    def hess(self, tij):
        n = len(tij)
        H = np.ones((n,n))*4*self.gencost_a + np.eye(n)*(2*self.gamma+self.rho)
        return H

    def trade_update(self, Phi):
        minbound = np.array([self.Pmin for i in range(len(self.omega_i))])
        maxbound = np.array([self.Pmax for i in range(len(self.omega_i))])
        bounds = Bounds(minbound, maxbound)

        linear_constraint = LinearConstraint(np.ones((1,len(self.omega_i))),\
            [self.Pmin], [self.Pmax])

        tij0 = [self.tij[k] for k in self.omega_i]
        res = minimize(self.objective_function, tij0, method='trust-constr',
            jac = self.jac, hess = self.hess,
            constraints = linear_constraint,
            options={'verbose':0, 'gtol':1e-10, 'xtol':1e-10}, 
            bounds=bounds,
            )
        # print(res.status, "\n")

        Tij = {}
        k = 0
        for j in range(N_Omega):
            if j in self.omega_i:
                if j in Phi:
                    self.tij[j] = res.x[k]
                    self.kij[j]+=1
                    Tij[j] = (res.x[k], self.kij[j])
                k = k+1

        return Tij


Agents = [Agent(i) for i in range(N_Omega)]

for i in range(N_Omega):
    Agents[i].initialize()

for k in range(30):
    print(("iteration", k))

    [Agents[i].price_update(Agents[i].omega_i) for i in range(N_Omega)]

    Tij = [Agents[i].trade_update(Agents[i].omega_i) for i in range(N_Omega)]

    Tji = [{j:Tij[j][i][0] for j in Agents[i].omega_i} for i in range(N_Omega)]

    [Agents[i].tji_update(Tji[i]) for i in range(N_Omega)]

T_final = np.array([Agents[i].tij for i in range(N_Omega)])
P_final = np.sum(T_final, axis=0)
