# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 17:49:11 2019

@author: Alyssia
"""

import numpy as np 
from scipy.optimize import minimize, Bounds, LinearConstraint

### Setting the agents data for the 'n3_p1_c2_0x0004' testcase.
agent_data = [
{"id_agent":0, "gencost_a":0.04610555780743429, "gencost_b":24.0,\
    "Pmin":0, "Pmax":539, "type_agent":"prod",\
    "loc_x":0.0664702923590903, "loc_y":0.6760277608975653},
{"id_agent":1, "gencost_a":0.02497954653252879, "gencost_b":76.0,\
    "Pmin":-714, "Pmax":0, "type_agent":"cons",\
    "loc_x":0.9605193832683507, "loc_y":0.9336155575980227},
{"id_agent":2, "gencost_a":0.03265879979275474, "gencost_b":78.0,\
    "Pmin":-123, "Pmax":0, "type_agent":"cons",\
    "loc_x":0.6310029533263766, "loc_y":0.1561816220663934}
]
N_Omega = len(agent_data)

### Agent class
class Agent:
    def __init__(self, id_agent):
        for agent in agent_data:
            if agent["id_agent"] == id_agent:
                self.type = agent["type_agent"]

                # Agent constants
                self.gencost_a = agent["gencost_a"]
                self.gencost_b = agent["gencost_b"]
                self.Pmin = agent["Pmin"]
                self.Pmax = agent["Pmax"]
                self.loc_x = agent["loc_x"]
                self.loc_y = agent["loc_y"]
        # Optimization parameters
        self.rho = 0.8
        self.gamma = 0.001#0.01
        self.epsilon_rel = 1e-4

        # Commercial links
        if self.type == "prod":
            partner_type = "cons"
        else:
            partner_type = "prod"
        omegai = [agent["id_agent"] for agent in agent_data\
            if agent["type_agent"]==partner_type]
        self.omega_i = set(omegai)
        self.dist = [np.sqrt((agent["loc_x"]-self.loc_x)**2\
            +(agent["loc_y"]-self.loc_y)**2)\
            for agent in agent_data]

    def __str__(self):
            return "%s   %s " % (self.gencost_a, self.Pmin)

    def initialize(self):
        self.tij = np.zeros(N_Omega)
        self.tji = np.zeros(N_Omega)
        self.lambdaij = np.zeros(N_Omega)
        self.kij = np.zeros(N_Omega, dtype = int)

    def tji_update(self, Tji_update):
        for j, tji in Tji_update.items():
            self.tji[j] = tji

    def price_update(self, Phi):
        for j in Phi:
            self.lambdaij[j] = self.lambdaij[j] -\
                self.rho/2*(self.tij[j]+self.tji[j])

    def objective_function(self, tij):
        pi = sum(tij)
        tij_k = np.array([self.tij[k] for k in self.omega_i])
        tji_k = np.array([self.tji[k] for k in self.omega_i])
        lambdaij_k = np.array([self.lambdaij[k] for k in self.omega_i])
        return 2*self.gencost_a*(pi**2) +\
            self.gencost_b*pi +\
            self.gamma*sum(np.square(tij)) +\
            self.rho/2*sum(np.square((tij_k-tji_k)/2-tij+lambdaij_k/self.rho))

    def jac(self, tij):
        tij_k = np.array([self.tij[k] for k in self.omega_i])
        tji_k = np.array([self.tji[k] for k in self.omega_i])
        lambdaij_k = np.array([self.lambdaij[k] for k in self.omega_i])

        d1 = 4*self.gencost_a*sum(tij)
        d2 = self.gencost_b
        d3 = 2*self.gamma*tij
        d4 = - self.rho*((tij_k-tji_k)/2-tij+lambdaij_k/self.rho)

        return d1+d2+d3+d4

    def hess(self, tij):
        n = len(tij)
        H = np.ones((n,n))*4*self.gencost_a + np.eye(n)*(2*self.gamma+self.rho)
        return H

    def trade_update(self, Phi):
        minbound = np.array([self.Pmin for i in range(len(self.omega_i))])
        maxbound = np.array([self.Pmax for i in range(len(self.omega_i))])
        bounds = Bounds(minbound, maxbound)

        linear_constraint = LinearConstraint(np.ones((1,len(self.omega_i))),\
            [self.Pmin], [self.Pmax])

        tij0 = [self.tij[k] for k in self.omega_i]
        res = minimize(self.objective_function, tij0, method='trust-constr',
            jac = self.jac, hess = self.hess,
            constraints = linear_constraint,
            options={'verbose':0, 'gtol':1e-10, 'xtol':1e-10}, 
            bounds=bounds,
            )
        # print(res.status, "\n")

        Tij = {}
        k = 0
        for j in range(N_Omega):
            if j in self.omega_i:
                if j in Phi:
                    self.tij[j] = res.x[k]
                    self.kij[j]+=1
                    Tij[j] = (res.x[k], self.kij[j])
                k = k+1

        return Tij


A0 = Agent(0)
A1 = Agent(1)
A2 = Agent(2)

A0.initialize()
A1.initialize()
A2.initialize()

for k in range(30):
    A0.price_update(A0.omega_i)
    A1.price_update(A1.omega_i)
    A2.price_update(A2.omega_i)

    T0j = A0.trade_update(A0.omega_i)
    T1j = A1.trade_update(A1.omega_i)
    T2j = A2.trade_update(A2.omega_i)

    Tj0 = {1:T1j[0][0], 2:T2j[0][0]}
    Tj1 = {0:T0j[1][0]}
    Tj2 = {0:T0j[2][0]}

    A0.tji_update(Tj0)
    A1.tji_update(Tj1)
    A2.tji_update(Tj2)