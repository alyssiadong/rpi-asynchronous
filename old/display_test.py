import matplotlib
matplotlib.use('TkAgg')

import random
import matplotlib.pyplot as plt 
import numpy as np

a = np.array([random.random() for i in range(20)])
fig = plt.plot(a)
plt.show(fig)