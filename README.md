# Asynchronous peer-to-peer market on a Raspberry Pi cluster

<img src="https://gitlab.com/gusj/opens/-/wikis/uploads/4af2f45f5b880a16967473c5836808be/logo400x206.png" width="400" height="220" />

## Introduction

### Market clearing algorithm

### Asynchronism
In a synchronous algorithm, each agent has to wait for every one of its trading partners' messages before performing its next iteration. Here, in an **asynchronous algorithm**, each agent only has to wait for a minimum number of message *N_trig* before triggering its next iteration.  

## Cluster test case

The testcase consists of **9 agents** (3 producers and 6 consumers) proceeding to a **market clearing** using a ***decentralized asynchronous peer-to-peer*** algorithm.  
Each agent is implemented on a seperate Raspberry Pi computer, and therefore has to communicate with its trading partners in order to solve the global problem.

<img src="/images/connection_diagram.png" width="350" height="300" />

### Communication
The agents are communicating with each other by publishing and subscribing to **MQTT topics**. Each published message is sent to a *broker* that redirects it to the clients that previously subscribed to the message's topic.  Here, each market agent is a MQTT client connected to the *mosquitto* broker of the user's machine. We used python's *paho-mqtt* client library.

### Output
During the resolution of the algorithm, the traded power variable is plotted on the local Raspberry Pi screen as a function of the local iteration counter.  


### State management

In order to manage the various layers (application layer, communication layer) of this project, we used a **state machine** from the python *transitions* package. This allows for error management.


## Quickstart

### Required hardware
- Raspberry Pi 3 Model B
- 4D Systems Screen: 4DPi-24-HAT

### Required python packages

- numpy
- scipy
- matplotlib
- pandas
- sympy
- paho-mqtt
- transitions
- threading
- osqp (requires gcc and cmake: https://solarianprogrammer.com/2017/12/08/raspberry-pi-raspbian-install-gcc-compile-cpp-17-programs/)

### Launching the script

Make sure the folder containing the codes are in all the Pis.
Make sure the MQTT broker is running on the main machine.  
Launch the Pi's bash script all at once (using for example MobaXTerm). This bash script starts by activating the screen, then launch the local agent's process.

> bash /home/pi/Documents/nine_agents/bin/launch_agent


## Code architecture

<img src="/images/code_architecture.png" width="600" height="200" />

The local agent process is contained in the state machine 'AgentStateMachine.py'. This state machine manages everything from the MQTT client connection, to the local agent iteration computations. It uses various functions contained in 'agent.py', 'mqtt_functions.py' and 'screen_buttons.py'.

### MQTT Client

*mqtt_functions.Connect()* creates a thread that: 
- connects to the MQTT broker,
- subscribes to the required topics,
- constantly puts received messages in the *MessageBuffer.queued_messages_in* queue,
- constantly sends messages contained in the *MessageBuffer.queued_messages_out* queue to the broker,
- tries to connect to the broker if it has been disconnected.
This thread can be killed by setting *MessageBuffer.tKill_clientLoop* to *True*.

### Message Buffer

This script contains global variables shared by 'AgentStateMachine.py' and 'mqtt_functions':
- *queued_messages_in*, *queued_messages_out* are queues containing messages,
- *tState_clientLoop*, *tState_checkConnection*, *tState_heartOut*, *tState_heartIn* are flags that indicates the state of the threaded functions,
- *tKill_clientLoop*, *tKill_checkConnection*, *tKill_heartOut*, *tKill_heartIn* are flags that, when set to *True*, kills the corresponding threads.
- *pause_flag* is a flag that indicates whether or not the local agent is paused. It is activated/desactivated by buttons press. 

### Agent
'agent.py' contains the *Agent* class that represents the local market agent with its local variables (t_ij, t_ji, lambda_ij) and the local methods (trade update, price update, residual computation). 

### Screen buttons
'screen_buttons.py' contains function to read the state of buttons on the 4D Systems display screen. 

### State machine
<img src="/images/state_machine.png" width="400" height="420" />

- **GlobalInit**: Initialize parameters and optimization variables
- **BrokerConnection**: Launch the threads that connects to the MQTT broker
    - Continues to the next state if the connection is successful
- **AgentInit**: Initializes the local agent
    - Sends out heartbeat every 5 seconds (*heartOut* thread)
    - Listens to the other agents' heartbeats and determines which agents are still connected (*heartIn* thread)
    - If all of the agent's trading partners are connected, sends the first iteration messages
- **Stall**: Waits for at least *N_trig* messages from the trading partners
    - Timeout : 60 seconds
- **Computation**: Updates the local variables with the newly arrived information and proceeds to do the iteration computation
- **Sendback**: Sends the updated local variables to the trading partners
- **Pause**: if the *pause_flag* has been set to True by a button press, the agent process stops and waits for the flag to be False again. The outgoing heartbeat messages are not sent during this state.

At each state, the default transition if all other transitions' conditions are False is a transition to the **Error** state. 

